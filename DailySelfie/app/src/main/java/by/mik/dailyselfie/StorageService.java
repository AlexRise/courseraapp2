package by.mik.dailyselfie;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StorageService {

    private File directory;

    public StorageService(String folderName){
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), folderName);
        file.mkdirs();
        this.directory =  file;
    }

    public List<PhotoItem> listFilesWithSubFolders() {
        List<PhotoItem> files = new ArrayList();
        if(directory.listFiles() != null) {
            for (File file : directory.listFiles()) {
                if (!file.isDirectory()) {
                    String absolutePath = file.getAbsolutePath();
                    String name = file.getName();
                    long lastModified = file.lastModified();
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap photo = BitmapFactory.decodeFile(absolutePath, options);

                    files.add(new PhotoItem(photo, name, absolutePath, lastModified));
                }
            }
        }
        return files;
    }

    public Uri generateFileUri() {

        File file  = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        return Uri.fromFile(file);
    }
}
