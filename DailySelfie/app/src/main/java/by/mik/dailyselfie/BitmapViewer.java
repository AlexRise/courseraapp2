package by.mik.dailyselfie;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

public class BitmapViewer extends AppCompatActivity {

    public static final String INTENT_PATH = "intent_path";

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_bitmap_viewer);
        Intent intent = getIntent ();
        String absolutePath = intent.getStringExtra (INTENT_PATH);
        ImageView image = (ImageView) findViewById (R.id.showSelfie);
        BitmapFactory.Options options = new BitmapFactory.Options ();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap original = BitmapFactory.decodeFile (absolutePath, options);
        image.setImageBitmap (original);
        Toast.makeText (this, absolutePath, Toast.LENGTH_SHORT).show ();
    }
}
