package by.mik.dailyselfie;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private final String MY_FOLDER = "MyFolder";
    final int REQUEST_CODE_PHOTO = 1;
    final String TAG = "myLogs";

    private List<PhotoItem> photoImgList;
    private BitmapAdapter bitmapAdapter;
    private ListView listView;
    private TextView textListEmpty;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private StorageService storageService;

    public static final String INTENT_PATH = "intent_path";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickPhoto();
            }
        });

        textListEmpty = (TextView) findViewById(R.id.textListEmpty);
        textListEmpty.setVisibility(View.GONE);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        storageService = new StorageService(MY_FOLDER);
        photoImgList = storageService.listFilesWithSubFolders();

        bitmapAdapter = new BitmapAdapter(this, photoImgList);
        listView = (ListView) findViewById(R.id.listPhoto);
        listView.setAdapter(bitmapAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PhotoItem selectedSelfie = (PhotoItem) bitmapAdapter.getItem(i);
                Intent showSelfie = new Intent(getBaseContext(), BitmapViewer.class);
                showSelfie.putExtra(INTENT_PATH, selectedSelfie.getAbsolutePath());

                startActivity(showSelfie);
            }
        });

        listView.setChoiceMode (ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener (new MyListener());

        scheduleNotification(getNotification("10 second delay"), 20000);
    }

    public void onClickPhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri fileUri = storageService.generateFileUri();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, REQUEST_CODE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == REQUEST_CODE_PHOTO) {
            if (resultCode == RESULT_OK) {
                if (intent == null) {
                    Log.d(TAG, "Intent is null");
                } else {
                    Log.d(TAG, "Photo uri: " + intent.getData());
                    Bundle bndl = intent.getExtras();
                    if (bndl != null) {
                        Object obj = intent.getExtras().get("data");
                        if (obj instanceof Bitmap) {
                            Bitmap bitmap = (Bitmap) obj;
                            Log.d(TAG, "bitmap " + bitmap.getWidth() + " x "
                                    + bitmap.getHeight());
                        }
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "Canceled");
            }
        }
        refreshPhotoLIst();
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        refreshPhotoLIst();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void refreshPhotoLIst() {
        photoImgList = storageService.listFilesWithSubFolders();
        bitmapAdapter.setBitmapList(photoImgList);
        bitmapAdapter.notifyDataSetChanged();
    }


    private void scheduleNotification(Notification notification, int delay) {

        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, delay, pendingIntent);
    }

    private Notification getNotification(String content) {
        PendingIntent activityPendingIntent = getActivityPendingIntent(this);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_text))
                .setSmallIcon(R.drawable.camera_icon)
                .setContentIntent(activityPendingIntent)
                .setCategory(Notification.CATEGORY_STATUS)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .build();

        return notification;
    }

    private PendingIntent getActivityPendingIntent(Context context) {
        Intent activityIntent = new Intent(context, MainActivity.class);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(context, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    private class MyListener implements AbsListView.MultiChoiceModeListener {

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            bitmapAdapter.toggleSelection (position);
            int selectedCount = bitmapAdapter.getSelectedItemsCount ();
            mode.setTitle (selectedCount + " Selected");
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater ().inflate (R.menu.context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
            if (item.getItemId () == R.id.action_delete) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder (MainActivity.this);
                alertDialog.setTitle (R.string.delete_photos);
                alertDialog.setMessage (R.string.delete_question);
                alertDialog = alertDialog.setPositiveButton (R.string.yes, new DialogInterface.OnClickListener () {
                    @Override
                    public void onClick (DialogInterface dialogInterface, int i) {
                        bitmapAdapter.removeSelectedItems ();
                        mode.finish ();
                    }
                });
                alertDialog.setNegativeButton (R.string.no, new DialogInterface.OnClickListener () {
                    @Override
                    public void onClick (DialogInterface dialogInterface, int i) {

                    }
                });
                alertDialog.show ();
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            bitmapAdapter.removeSelections ();
            mode = null;
        }
    }
}
