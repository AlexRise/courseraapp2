package by.mik.dailyselfie;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BitmapAdapter extends BaseAdapter {

    private Context mContext;
    private List<PhotoItem> photoItemList;
    private ArrayList<PhotoItem> selectedPhotoItems;
    private LayoutInflater lInflater;

    public BitmapAdapter(Context context , List<PhotoItem> photoItemList) {
        this.mContext = context;
        this.photoItemList = photoItemList;
        this.selectedPhotoItems = new ArrayList<PhotoItem> ();
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return photoItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return photoItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setBitmapList(List<PhotoItem> photoItemList){
        this.photoItemList = photoItemList;
    }

    public void toggleSelection (int position) {
        PhotoItem photoItem = photoItemList.get (position);
        boolean found = isSelected (photoItem);
        if (!found) {
            selectedPhotoItems.add (photoItem);
        } else {
            selectedPhotoItems.remove (photoItem);
        }
        this.notifyDataSetChanged ();
    }

    private boolean isSelected (PhotoItem photoItem ) {
        boolean found = false;
        for (PhotoItem iter : selectedPhotoItems) {
            if (iter.equals (photoItem)) {
                found = true;
                break;
            }
        }
        return found;
    }

    public int getSelectedItemsCount () {
        return selectedPhotoItems.size ();
    }

    public void removeSelectedItems () {
        for (PhotoItem item : selectedPhotoItems) {
            File file = new File (item.getAbsolutePath ());
            if (!file.delete ()) {
                Toast.makeText (mContext, file.getName () + " can not be removed!", Toast.LENGTH_SHORT).show ();
            }
            photoItemList.remove (item);
        }
        selectedPhotoItems.clear ();
        this.notifyDataSetChanged ();
    }

    public void removeSelections () {
        selectedPhotoItems.clear ();
        this.notifyDataSetChanged ();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = lInflater.inflate(R.layout.item_photo, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PhotoItem photoItem = (PhotoItem) getItem(position);

        viewHolder.namePhoto.setText(photoItem.getName());
        viewHolder.photo.setImageBitmap(photoItem.getPhoto());

        if (isSelected (photoItem)) {
            convertView.setBackgroundColor (0x9934B5E4);
        } else {
            convertView.setBackgroundColor (Color.TRANSPARENT);
        }

        return convertView;
    }

    private class ViewHolder {

        private TextView namePhoto;
        private ImageView photo;

        public ViewHolder(View convertView) {

            namePhoto = (TextView) convertView.findViewById(R.id.namePhoto);
            photo = (ImageView) convertView.findViewById(R.id.imageViewPriority);
        }
    }
}
