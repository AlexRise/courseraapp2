package by.mik.dailyselfie;


import android.graphics.Bitmap;

public class PhotoItem {

    private Bitmap photo;
    private String name;
    private String absolutePath;
    private long lastChange;

    public PhotoItem(Bitmap photo, String name, String absolutePath, long lastChange) {
        this.photo = photo;
        this.name = name;
        this.absolutePath = absolutePath;
        this.lastChange = lastChange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public long getLastChange() {
        return lastChange;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }
}
